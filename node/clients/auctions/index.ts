import { ExternalClient, InstanceOptions, IOContext } from '@vtex/api'

export class auctionsClient extends ExternalClient {

  constructor(context: IOContext, options?: InstanceOptions) {
    super('http://httpstat.us', context, options)
  }

  public auctions = () => true

  public auctionsProducts= (auctionsProducts: any) => auctionsProducts

  public auctionsConfig= () => true

}
