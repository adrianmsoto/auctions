export const auctions = async (
   _: any,
   { clients: { auctions: auctionsClient } }: Context
 ) => auctionsClient.auctions()