export const auctionsProducts = async (
   _: any,
   { auctionsProducts }: any,
   { clients: { auctions: auctionsClient } }: Context
 ) => auctionsClient.auctionsProducts(auctionsProducts)