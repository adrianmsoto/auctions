export const auctionsConfig = async (
   _: any,
   { clients: { auctions: auctionsClient } }: Context
 ) => auctionsClient.auctionsConfig()